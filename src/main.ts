import { createApp } from 'vue'
import App from './App.vue'

import store from './store'

const app: any = createApp(App)

app.use(store)
app.mount('#app')
