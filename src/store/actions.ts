const run: any = ({ commit }: any, payload: any) => commit("run", payload);
const open: any = ({ commit }: any, payload: any) => commit("open", payload);
const page: any = ({ commit }: any, payload: any) => commit("page", payload);
const count: any = ({ commit }: any, payload: any) => commit("count", payload);
const lesson: any = ({ commit }: any, payload: any) => commit("lesson", payload);
export default {
  run,
  open,
  page,
  count,
  lesson,
};