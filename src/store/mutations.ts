import { api, emit } from "@/utils";
const { get, post } = emit;
export default {
  count (state: any, payload: any) {
    const { callback } = payload;
    get('/lesson/count', callback);
  },
  lesson (state: any, payload: any) {
    const { id, callback } = payload;
    get(`/lesson/${id}`, callback);
  },
  run (state: any, payload: any) {
    const { code, callback } = payload;
    post(api, code, callback);
  },
  open (state: any, payload: any) {
    const { callback } = payload;
    state.open = callback;
  },
  page (state: any, payload: any) {
    const { feature } = payload;
    if (state.open) state.open(feature);
    state.info[feature] = !state.info[feature];
  },
};